-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 12 oct. 2020 à 16:58
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `polpot`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `ClientID` int(11) NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`ClientID`, `Nom`, `Prenom`) VALUES
(1, 'Lenom', 'Paul'),
(2, 'Lanom', 'Manon'),
(3, 'Lonom', 'Martin'),
(4, 'Lunom', 'Lucie');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `CommandeID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `DateCommande` date NOT NULL,
  `QuantitéProduit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`CommandeID`, `ClientID`, `DateCommande`, `QuantitéProduit`) VALUES
(1, 1, '2020-10-08', 20),
(2, 2, '2020-10-06', 10),
(3, 3, '2020-06-08', 5),
(4, 4, '2020-01-06', 10);

-- --------------------------------------------------------

--
-- Structure de la table `creationdevis`
--

CREATE TABLE `creationdevis` (
  `CreationDevisID` int(11) NOT NULL,
  `CommandeID` int(11) NOT NULL,
  `DevisID` int(11) NOT NULL,
  `DateDevis` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `creationdevis`
--

INSERT INTO `creationdevis` (`CreationDevisID`, `CommandeID`, `DevisID`, `DateDevis`) VALUES
(1, 1, 1, '2020-10-10'),
(2, 2, 2, '2020-10-31');

-- --------------------------------------------------------

--
-- Structure de la table `creationfacture`
--

CREATE TABLE `creationfacture` (
  `CreationfactureID` int(11) NOT NULL,
  `factureID` int(11) NOT NULL,
  `productionID` int(11) NOT NULL,
  `datefacture` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `creationfacture`
--

INSERT INTO `creationfacture` (`CreationfactureID`, `factureID`, `productionID`, `datefacture`) VALUES
(1, 1, 1, '2020-10-12'),
(2, 2, 2, '2020-10-13');

-- --------------------------------------------------------

--
-- Structure de la table `devis`
--

CREATE TABLE `devis` (
  `DevisID` int(11) NOT NULL,
  `DevisQuantité` int(11) NOT NULL,
  `DevisPrix` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `devis`
--

INSERT INTO `devis` (`DevisID`, `DevisQuantité`, `DevisPrix`) VALUES
(1, 20, 200),
(2, 10, 100);

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

CREATE TABLE `facture` (
  `FactureID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `FacturePrix` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `facture`
--

INSERT INTO `facture` (`FactureID`, `ClientID`, `FacturePrix`) VALUES
(1, 1, 200),
(2, 2, 100),
(3, 1, 200),
(4, 2, 100);

-- --------------------------------------------------------

--
-- Structure de la table `production`
--

CREATE TABLE `production` (
  `ProductionID` int(11) NOT NULL,
  `ProduitID` int(11) NOT NULL,
  `CommandeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `production`
--

INSERT INTO `production` (`ProductionID`, `ProduitID`, `CommandeID`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `ProduitID` int(11) NOT NULL,
  `ProduitNom` varchar(50) NOT NULL,
  `PrixProduit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`ProduitID`, `ProduitNom`, `PrixProduit`) VALUES
(1, 'pp', 10);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`ClientID`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`CommandeID`);

--
-- Index pour la table `creationdevis`
--
ALTER TABLE `creationdevis`
  ADD PRIMARY KEY (`CreationDevisID`);

--
-- Index pour la table `creationfacture`
--
ALTER TABLE `creationfacture`
  ADD PRIMARY KEY (`CreationfactureID`);

--
-- Index pour la table `devis`
--
ALTER TABLE `devis`
  ADD PRIMARY KEY (`DevisID`);

--
-- Index pour la table `facture`
--
ALTER TABLE `facture`
  ADD PRIMARY KEY (`FactureID`);

--
-- Index pour la table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`ProductionID`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`ProduitID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `ClientID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `CommandeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `creationdevis`
--
ALTER TABLE `creationdevis`
  MODIFY `CreationDevisID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `creationfacture`
--
ALTER TABLE `creationfacture`
  MODIFY `CreationfactureID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `devis`
--
ALTER TABLE `devis`
  MODIFY `DevisID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `facture`
--
ALTER TABLE `facture`
  MODIFY `FactureID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `production`
--
ALTER TABLE `production`
  MODIFY `ProductionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `ProduitID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
